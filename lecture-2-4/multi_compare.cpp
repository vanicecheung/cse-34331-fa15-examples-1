#include <algorithm>
#include <iostream>
#include <string>

using namespace std;

typedef struct {
    string first_name;
    string last_name;
} Student;

const size_t NSTUDENTS = 6;

Student students[NSTUDENTS] = {
    {"Steve" , "Nguyen"},
    {"Phu"   , "Nguyen"},
    {"Jack"  , "Johnson"},
    {"Chris" , "Johnson"},
    {"Robert", "Smith"},
    {"John"  , "Smith"},
};

void print_students(Student a[], size_t n) {
    for_each(a, a + n, [](Student &s) { cout << s.last_name << ", " << s.first_name << endl; });
    cout << endl;
}

bool compare_students(const Student &a, const Student &b) {
    if (a.last_name == b.last_name) 
	return a.first_name < b.first_name;

    return a.last_name < b.last_name;
}

int main(int argc, char *argv[]) {
    print_students(students, NSTUDENTS);
    
    sort(students, students + NSTUDENTS, compare_students);
    print_students(students, NSTUDENTS);

    return 0;
}
