#ifndef VECTOR_STACK
#define VECTOR_STACK

#include <vector>

template <typename T>
class vector_stack {
    std::vector<T> data;

    public:
	bool empty()   const { return data.empty(); }
	const T& top() const { return data.back(); }
	void push(const T& value) {
	    data.push_back(value);
	}
	void pop() {
	    data.pop_back();
	}
};

#endif
