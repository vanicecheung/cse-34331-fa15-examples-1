#include <algorithm>
#include <iostream>
#include <set>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    set<string> names;

    names.insert("Peter");
    names.insert("James");
    names.insert("Bui");
    names.insert("Peter");
    names.insert("James");

    cout << "Size:            " << names.size() << endl;

    cout << "Elements:       "; 
    for_each(names.begin(), names.end(), [](string n){cout << " " << n;});
    cout << endl;

    cout << "Search(Peter):   " 
    	 << (names.find("Peter") != names.end() ? "Found" : "Not Found") << endl;
    
    cout << "Search(Butters): " 
    	 << (names.find("Butters") != names.end() ? "Found" : "Not Found") << endl;

    return 0;
}
