#include <iostream>
#include <memory>
#include <vector>

using namespace std;

template <typename T>
struct Node {
    T value;
    struct Node *left;
    struct Node *right;
};

template <typename T>
bool search(Node<T> *root, const T& target) {
    if (root == nullptr)
    	return false;

    if (root->value == target) {
    	return true;
    }

    if (target < root->value)
    	return search(root->left, target);
    else
    	return search(root->right, target);
}

template <typename T>
Node<T>* insert(Node<T> *root, const T& value) {
    if (root == nullptr)
    	return new Node<T>{value, nullptr, nullptr};

    if (value <= root->value) {
    	if (root->left == nullptr) {
    	    root->left = new Node<T>{value, nullptr, nullptr};
	} else {
	    insert(root->left, value);
	}
    } else {
    	if (root->right == nullptr) {
    	    root->right = new Node<T>{value, nullptr, nullptr};
	} else {
	    insert(root->right, value);
	}
    }

    return root;
}

template <typename T>
void print(Node<T> *root, const string &indent) {
    if (root == nullptr)
    	return;

    print(root->right, indent + indent);
    cout << indent << root->value << endl;
    print(root->left, indent + indent);
}

int main(int argc, char *argv[]) {
    Node<int> *bst0 = new Node<int>{8,
    	new Node<int>{3,
    	    new Node<int>{1, nullptr, nullptr},
    	    new Node<int>{6,
		new Node<int>{4, nullptr, nullptr},
		new Node<int>{7, nullptr, nullptr},
	    },
	},
    	new Node<int>{10,
    	    nullptr,
    	    new Node<int>{14,
		new Node<int>{13, nullptr, nullptr},
		nullptr,
    	    },
	},
    };

    cout << "BST0" << endl;
    print(bst0, " ");
    for (auto value : vector<int>{8, 3, 1, 6, 4, 7, 10, 14, 13, -1, 100}) {
	cout << "Search(" << value << "): " << search(bst0, value) << endl;
    }

    Node<int> *bst1 = nullptr;
    for (auto value : vector<int>{8, 3, 1, 6, 4, 7, 10, 14, 13}) {
    	bst1 = insert(bst1, value);
    }
    cout << "BST1" << endl;
    print(bst1, " ");
    for (auto value : vector<int>{8, 3, 1, 6, 4, 7, 10, 14, 13, -1, 100}) {
	cout << "Search(" << value << "): " << search(bst1, value) << endl;
    }

    return 0;
}
