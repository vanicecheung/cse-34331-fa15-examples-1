// duplicates_linear.cpp: simple duplicates check

#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <set>
#include <vector>

const int NITEMS = 1<<15;

int main(int argc, char *argv[]) {
    std::set<int> s;

    srand(time(NULL));

    for (int i = 0; i < NITEMS; i++) {
    	int n = rand();

	if (s.find(n) == s.end()) {
	   s.insert(n);
	} else {
	    std::cout << n << " is duplicated!" << std::endl;
	    break;
	}
    }

    return 0;
}
