#ifndef ARRAY_H
#define ARRAY_H

#include <algorithm>

template <typename T>
class Array {
    public:
	Array(T n);			    // Constructor
	~Array();			    // Destructor
	T& operator[](int i);		    // Index operator
	int size() const;

	Array(const Array<T>& source);	    // Copy Constructor
	void operator=(const Array<T>& source);// Assignment Operator

    private:
    	T*  data;
    	int length;
};

template <typename T>
Array<T>::Array(T n)
{ 
    data   = new T[n];
    length = n;
}

template <typename T>
Array<T>::~Array()
{ 
    delete [] data;
}

template <typename T>
T& Array<T>::operator[](int i)
{ 
    return data[i];
}

template <typename T>
int Array<T>::size() const
{
    return length;
}

template <typename T>
Array<T>::Array(const Array<T> &source)
{ 
    data   = new T[source.length];
    length = source.length;
    std::copy(source.data, source.data + length, data);
}

template <typename T>
void Array<T>::operator=(const Array<T> &source)
{ 
    if (this == &source) {
    	return;
    }
    
    delete [] data;
    data   = new T[source.length];
    length = source.length;
    std::copy(source.data, source.data + length, data);
}

#endif
