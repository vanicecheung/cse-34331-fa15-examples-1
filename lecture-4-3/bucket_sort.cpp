#include <algorithm>
#include <cassert>
#include <iostream>
#include <random>
#include <vector>

using namespace std;

#define MIN_VALUE   (0)
#define MAX_VALUE   (1000000)

template <typename T>
void bucket_sort(vector<T> &v, size_t nbuckets) {
    // Initialize buckets
    vector<vector<T>> buckets(nbuckets + 1);
    size_t bucket_size = MAX_VALUE / nbuckets;

    // Distribute v values into buckets
    for (auto i : v) {
    	buckets[(size_t)(i) / bucket_size].push_back(i);
    }

    // Sort buckets and copy values back to v vector
    size_t index = 0;
    for (auto bucket : buckets) {
    	sort(bucket.begin(), bucket.end());
    	for (auto item : bucket) {
    	    v[index++] = item;
	}
    }
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
    	cerr << "usage: " << argv[0] << " nitems nbuckets" << endl;
    	return EXIT_FAILURE;
    }

    size_t nitems   = atoi(argv[1]);
    size_t nbuckets = atoi(argv[2]);

    vector<double> v(nitems);

    default_random_engine generator;
    uniform_int_distribution<int> distribution(MIN_VALUE, MAX_VALUE);

    for (size_t i = 0; i < nitems; i++) {
    	v[i] = distribution(generator);
    }

    bucket_sort(v, nbuckets);

    for (size_t i = 1; i < nitems; i++) {
    	assert(v[i] >= v[i - 1]);
    }

    return EXIT_SUCCESS;
}
